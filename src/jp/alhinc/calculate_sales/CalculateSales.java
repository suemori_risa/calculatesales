package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String FILE_CONSECUTIVE_NUMBERS = "売り上げファイル名が連番になっていません";
	private static final String OVER_10_DIGITS ="合計金額が10桁を超えました";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String INVALID_COMMODITY_CODE = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数S
	 */

	public static void main(String[] args){
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと合計金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		String regex  ="^\\d{3}$";
		String type = "支店定義";
		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, regex, type)) {
			return;
		}

		regex = "^[A-Za-z0-9]{8}$";
		type = "商品定義";
		// 商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, regex, type)) {
			return;
		}

		//売り上げファイルの抽出
		//listFilesメソッドによりパス内のすべてのファイルが格納
		File[] salesfiles = new File(args[0]).listFiles();
		//リストを作成
		List<File> rcdFiles = new ArrayList<>();

		//売り上げファイルをすべて取得し8桁かつrcdで終わるファイルのみリストに追加する
		for(int i = 0; i < salesfiles.length; i++) {
			if(salesfiles[i].isFile() && salesfiles[i].getName().matches("^\\d{8}.rcd$")){
				rcdFiles.add(salesfiles[i]);
			}
		}

		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0 , 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0 , 8));

			if((latter - former) != 1) {
				System.out.println(FILE_CONSECUTIVE_NUMBERS);
				return;
			}
		}

		//ファイルを１つずつ読み込み、集計
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				// 一行ずつ読み込む
				List<String> sales = new ArrayList<>();

				while((line = br.readLine()) != null) {
					sales.add(line);
				}

				if(sales.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_FORMAT);
					return;
				}

				if(!branchNames.containsKey(sales.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_BRANCH_CODE);
					return;
				}

				if(!commodityNames.containsKey(sales.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_COMMODITY_CODE);
					return;
				}

				if(!sales.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売り上げファイルの金額をLong型に変換
				long fileSale = Long.parseLong(sales.get(2));

				//売り上げファイルの支店コードに対応したキーから、初期化した0の値＋売上金額
				Long saleAmount = branchSales.get(sales.get(0)) + fileSale;

				Long saleCommodityAmount = commoditySales.get(sales.get(1)) + fileSale;

				if((saleAmount >= 10000000000L)||(saleCommodityAmount >= 10000000000L)){
					System.out.println(OVER_10_DIGITS);
					return;
				}

				branchSales.put(sales.get(0), saleAmount);
				commoditySales.put(sales.get(1), saleCommodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */

	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String regex, String type) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			//ファイルの存在チェック
			if(!file.exists()) {
				System.out.println(type + FILE_NOT_EXIST);
				//readFileメソッドがbooleanだから
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if((items.length != 2)||(!items[0].matches(regex))){
					System.out.println(type + FILE_INVALID_FORMAT);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */

	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;

		try {
			File allBranchFile = new File(path, fileName);
			FileWriter fw = new FileWriter(allBranchFile);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
